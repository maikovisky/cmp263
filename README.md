# CMP263 - Aprendizagem de Máquina (2021/1)

Disciplina - CMP263 - Aprendizagem de Máquina (2021/1) - Mestrado UFRGS




## Exercícios

Utilizado Jupyter-lab em cada um dos exercicios

- [Algoritimo KNN](algoritmo-knn).
- [Arvore de Decisão](arvore-decisao).
- [Naive-Bayes](naive-bayes).

