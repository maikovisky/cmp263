```python
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import tree
import graphviz 
import pandas
from graphviz import Source
```


```python
diabetes =  pandas.read_csv("diabetes.csv")
X = diabetes.iloc[:, :-1]
y = diabetes.Outcome

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=.2)

```


```python
clf = DecisionTreeClassifier(criterion="gini")
clf.fit(X_train, y_train)
graph = Source( tree.export_graphviz(clf, out_file=None, feature_names=X.columns))
graph.format = 'png'
graph.render('tree_gini',view=True)
y_predict = clf.predict(X_test)
accuracy_score(y_test, y_predict)

```




    0.6813008130081301




```python
clf = DecisionTreeClassifier(criterion="entropy")
clf.fit(X_train, y_train)
graph = Source( tree.export_graphviz(clf, out_file=None, feature_names=X.columns))
graph.format = 'png'
graph.render('tree_entropy',view=True)
y_predict = clf.predict(X_test)
accuracy_score(y_test, y_predict)

```




    0.6715447154471544




```python

```


```python

```
