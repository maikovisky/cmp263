import pandas

def normalize(df):
    return (df - df.min()) / ( df.max() - df.min())

def euclidean_distance(train, test):
    return (train - test)**2


k = 1
prop = 0.8

breast = pandas.read_csv("breast_cancer_data.csv").iloc[:,1:]

n = len(breast)

breast_train = breast[:int(n*prop)]
breast_test  = breast[int(n*prop):]

breastN_train = normalize(breast_train)
breastN_test  = normalize(breast_test)