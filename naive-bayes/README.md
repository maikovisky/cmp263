```python
import pandas
import numpy as np
```


```python
nb = pandas.read_csv("naive-bayes.csv", sep=";")
target = nb.target.value_counts()
nb_len = len(nb.index)
nb
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>price</th>
      <th>lug_boot</th>
      <th>safety</th>
      <th>target</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>low</td>
      <td>med</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>1</th>
      <td>low</td>
      <td>med</td>
      <td>high</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>2</th>
      <td>low</td>
      <td>small</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>3</th>
      <td>med</td>
      <td>small</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>4</th>
      <td>med</td>
      <td>big</td>
      <td>high</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>5</th>
      <td>high</td>
      <td>small</td>
      <td>high</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>6</th>
      <td>low</td>
      <td>big</td>
      <td>high</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>7</th>
      <td>high</td>
      <td>med</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>8</th>
      <td>med</td>
      <td>med</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>9</th>
      <td>med</td>
      <td>big</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>10</th>
      <td>low</td>
      <td>big</td>
      <td>med</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>11</th>
      <td>high</td>
      <td>big</td>
      <td>high</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>12</th>
      <td>low</td>
      <td>small</td>
      <td>med</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>13</th>
      <td>med</td>
      <td>med</td>
      <td>high</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>14</th>
      <td>med</td>
      <td>big</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>15</th>
      <td>low</td>
      <td>med</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>16</th>
      <td>low</td>
      <td>small</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>17</th>
      <td>med</td>
      <td>med</td>
      <td>med</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>18</th>
      <td>med</td>
      <td>med</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>19</th>
      <td>med</td>
      <td>small</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>20</th>
      <td>med</td>
      <td>big</td>
      <td>med</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>21</th>
      <td>high</td>
      <td>big</td>
      <td>med</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>22</th>
      <td>high</td>
      <td>big</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>23</th>
      <td>high</td>
      <td>small</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>24</th>
      <td>high</td>
      <td>small</td>
      <td>med</td>
      <td>unacc</td>
    </tr>
  </tbody>
</table>
</div>



### a. A priori, isto é,  sem considerar os atributos de cada instância, é menos provável que uma nova instância seja da classe acc do que da classe unacc.


```python
unacc = target.unacc/nb_len
acc = target.acc/nb_len
nb_len, unacc, acc
```




    (25, 0.6, 0.4)



Sendo **unacc (0.6) > acc (0.4)** então é provavel que uma nova instância é **unacc**

**Resposta: Verdadeiro**

------------
### b. O modelo Naïve Bayes considera que os atributos price, lug_boot e safety são estatisticamente independentes uns dos outros condicionado ao valor da classe (acc ou unacc).

**Resposta: Verdadeiro**

--------------
### c. A probabilidade condicional do atributo lug_boot igual à "med" é maior para a classe acc do que para a classe unacc. Isto é, P(lug_boot=med|target=acc) > P(lug_boot=med|target=unacc).



```python
lb = nb[(nb.lug_boot == 'med')]
lb_target = lb.target.value_counts()
lb_len = len(lb.index)
lb
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>price</th>
      <th>lug_boot</th>
      <th>safety</th>
      <th>target</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>low</td>
      <td>med</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>1</th>
      <td>low</td>
      <td>med</td>
      <td>high</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>7</th>
      <td>high</td>
      <td>med</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>8</th>
      <td>med</td>
      <td>med</td>
      <td>med</td>
      <td>acc</td>
    </tr>
    <tr>
      <th>13</th>
      <td>med</td>
      <td>med</td>
      <td>high</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>15</th>
      <td>low</td>
      <td>med</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>17</th>
      <td>med</td>
      <td>med</td>
      <td>med</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>18</th>
      <td>med</td>
      <td>med</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
  </tbody>
</table>
</div>




```python
lb_unacc = lb_target.unacc/target.unacc
lb_acc = lb_target.acc/target.acc
lb_len, lb_unacc, lb_acc
```




    (8, 0.26666666666666666, 0.4)



Sendo: 
> P(lug_boot=med|target=acc) = 0.4 

> P(lug_boot=med|target=unacc) = 0.2666

Então: **P(lug_boot=med|target=acc) > P(lug_boot=med|target=unacc)**

**Resposta: Verdadeiro**

----------
### d. Nenhuma instância com o atributo safety = low seria predita como acc (aceitável) por este classificador pelo problema da probabilidade zero. Isto é, com base nestes dados de treinamento, P(safety=low|target=acc) = 0.


```python
sf = nb[(nb.safety == 'low')]
sf_target = sf.target.value_counts()
sf_len = len(sf.index)
sf
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>price</th>
      <th>lug_boot</th>
      <th>safety</th>
      <th>target</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>14</th>
      <td>med</td>
      <td>big</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>15</th>
      <td>low</td>
      <td>med</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>16</th>
      <td>low</td>
      <td>small</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>18</th>
      <td>med</td>
      <td>med</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>19</th>
      <td>med</td>
      <td>small</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>22</th>
      <td>high</td>
      <td>big</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
    <tr>
      <th>23</th>
      <td>high</td>
      <td>small</td>
      <td>low</td>
      <td>unacc</td>
    </tr>
  </tbody>
</table>
</div>




```python
try:
    sf_unacc = sf_target.unacc/target.unacc
except AttributeError:
    sf_unacc = 0
    
try:
    sf_acc = sf_target.acc/target.acc
except AttributeError:
    sf_acc = 0
    
sf_len, sf_unacc, sf_acc
```




    (7, 0.4666666666666667, 0)



Sendo P(safety=low|target=acc) = 0

**Resposta verdadeira**

-----
### e. As estimativas das probabilidades a posteriori para uma nova instância x com atributos price=high, lug_boot=med e safety=med seriam aproximadamente P(target=acc|x) = 0.019  e  P(target=unacc|x) = 0.021. Portanto, esta instância será classificada como unacc pelo modelo.


```python
prod = pandas.DataFrame(data={"acc": [1], "unacc": [1]})
prod_acc   = 1
prod_unacc = 1

test = pandas.DataFrame(data={"price": ["high"], "lug_boot": ["med"], "safety": ["med"]})
for c in test.columns:
    p = nb[nb[c] == test[c][0]]
    p_target   = p.target.value_counts()
    prod_unacc = prod_unacc * p_target.unacc/target.unacc
    prod_acc   = prod_acc * p_target.acc/target.acc

Pacc_x   = prod_acc * acc
Punacc_x = prod_unacc * unacc

Pacc_x, Punacc_x
```




    (0.019200000000000002, 0.02133333333333333)



As estimativa de apriori estão com valores corretos, portanto a nova instância será classificada como unacc. 
**Resposta: verdadeira**
