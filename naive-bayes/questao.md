Analise os dados e as afirmativas abaixo e indique quais destas se refere a uma afirmativa **INCORRETA**. Nos seus cálculos, assuma os dados como são mostrados abaixo, sem a correção de Laplace.
 
Atenção: não devem ser utilizadas implementações prontas do Algoritmo Naïve Bayes neste exercício. O aluno deve anexar o código ou anotações digitalizadas para comprovar o cálculo das probabilidades envolvidas na questão no link adicional disponibilizado pela professora.
Escolha uma opção:

a. A priori, isto é,  sem considerar os atributos de cada instância, é menos provável que uma nova instância seja da classe acc do que da classe unacc.

b. O modelo Naïve Bayes considera que os atributos price, lug_boot e safety são estatisticamente independentes uns dos outros condicionado ao valor da classe (acc ou unacc).

c. A probabilidade condicional do atributo lug_boot igual à "med" é maior para a classe acc do que para a classe unacc. Isto é, P(lug_boot=med|target=acc) > P(lug_boot=med|target=unacc).

d. Nenhuma instância com o atributo safety = low seria predita como acc (aceitável) por este classificador pelo problema da probabilidade zero. Isto é, com base nestes dados de treinamento, P(safety=low|target=acc) = 0.

e. As estimativas das probabilidades a posteriori para uma nova instância x com atributos price=high, lug_boot=med e safety=med seriam aproximadamente P(target=acc|x) = 0.019  e  P(target=unacc|x) = 0.021. Portanto, esta instância será classificada como unacc pelo modelo.

f. Nenhuma das anteriores